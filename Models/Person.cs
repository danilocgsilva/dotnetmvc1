namespace dotnetmvc1.Models
{
    public class Person
    {
        public int Id { get; }
        public string name { get; set; }
        public int age { get; set; }
        public string email { get; set; }
    }
}